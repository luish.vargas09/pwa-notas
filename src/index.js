import React from 'react';
import ReactDOM from 'react-dom/client';
import AppNotas from './AppNotas';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <>
        <div className='container'>
            <AppNotas />
        </div>
    </>
);
