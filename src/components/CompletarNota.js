import React from 'react';

const CompletarNota = ({ nota, notaCompletada }) => {
    return (
        <>
            <input
                type='checkbox'
                className='m-2'
                checked={nota.done}
                onChange={() => notaCompletada(nota)}
                onClick={console.log}
            />
            Completada
        </>
    );
};

export default CompletarNota;
