// import { useState } from 'react';
import Item from './Item';

const Lista = ({
    notas,
    notaCompletada,
    mostrarCompletado = false,
    borrarNota,
}) => {
    const notaFila = (valorCompletada) => {
        return notas
            .filter((nota) => nota.done === valorCompletada)
            .map((nota) => (
                <Item
                    nota={nota}
                    key={nota.id}
                    notaCompletada={notaCompletada}
                    borrarNota={borrarNota}
                    notas={notas}
                />
            ));
    };

    return (
        <div>
            <table className='table table-striped'>
                <thead className='thead-light text-center table-dark '>
                    <tr>
                        {!mostrarCompletado ? (
                            <th>COSAS POR HACER</th>
                        ) : (
                            <th>COSAS COMPLETADAS</th>
                        )}
                    </tr>
                </thead>
                <tbody>{notaFila(mostrarCompletado)}</tbody>
            </table>
        </div>
    );
};

export default Lista;
