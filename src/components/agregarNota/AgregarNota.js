import React from 'react';
import { useState } from 'react';
import './AgregarNota.css';

const AgregarNota = ({ crearNota }) => {
    // const [nuevoTitulo, setNuevoTitulo] = useState('');
    const [nuevaNota, setNuevaNota] = useState('');

    const envioForm = (e) => {
        // Evito que se refresque la página
        e.preventDefault();

        // Creamos la nota
        crearNota(nuevaNota);

        // Dejo los inputs en blanco, listos para ser usados nuevamente.
        // setNuevoTitulo('');
        setNuevaNota('');
    };

    return (
        <>
            <h1 id='titulo'>AGENDA DE NOTAS</h1>
            <hr />
            <form onSubmit={envioForm}>
                {/* <input
                    type='text'
                    placeholder='Título'
                    value={nuevoTitulo}
                    onChange={(e) => setNuevoTitulo(e.target.value)}
                />
                <br /> */}
                <div className='text-center'>
                    <textarea
                        id='inputAreaText'
                        type='text'
                        placeholder='Escribe tu nota...'
                        value={nuevaNota}
                        onChange={(e) => setNuevaNota(e.target.value)}
                    />
                    <br />
                    <button id='botonAgregarNota' className='btn btn-success'>
                        Guardar
                    </button>
                </div>
                <br />
            </form>
            <hr />
        </>
    );
};

export default AgregarNota;
