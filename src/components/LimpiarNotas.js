import React from 'react';

const LimpiarNotas = ({
    setMostrarCompletado,
    limpiarNotasHechas,
    isChecked,
}) => {
    const eliminarNotas = () => {
        if (
            window.confirm(
                '¿Estás seguro de querer eliminar las notas completadas?'
            )
        ) {
            limpiarNotasHechas();
        }
    };

    return (
        <div className='my-4 text-center'>
            <input
                checked={isChecked}
                type='checkbox'
                onChange={(e) => setMostrarCompletado(e.target.checked)}
            />{' '}
            <label className='text-white'>Mostrar tareas completadas</label>{' '}
            <br />
            <button className='btn btn-primary my-3' onClick={eliminarNotas}>
                Limpiar
            </button>
        </div>
    );
};

export default LimpiarNotas;
