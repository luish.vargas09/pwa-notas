import React, { useState } from 'react';

const Buscador = () => {
    const [valorBuscado, setValorBuscado] = useState('');

    const buscar = (event) => {
        setValorBuscado(event.target.value);
        return valorBuscado;
    };

    return (
        <div>
            <input
                placeholder='Buscar una nota'
                className='buscador'
                onChange={buscar}
            />{' '}
            <button>Ir</button>
        </div>
    );
};

export default Buscador;
