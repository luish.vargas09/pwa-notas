import React from 'react';
import CompletarNota from './CompletarNota';
import EliminarNota from './EliminarNota';

const Item = ({ nota, notaCompletada, borrarNota }) => {
    console.log(nota);
    return (
        <>
            <tr>
                <td className='text-center'>
                    {/* <h4 className='text-white'>Título</h4> */}
                    {/* <br /> */}
                    <h6 className='text-white m-4'>{nota.name}</h6>
                    <div className='text-white mt-4 mb-1'>
                        <CompletarNota
                            nota={nota}
                            notaCompletada={notaCompletada}
                        />
                        <EliminarNota borrarNota={borrarNota} nota={nota} />
                    </div>
                </td>
            </tr>
        </>
    );
};

export default Item;
