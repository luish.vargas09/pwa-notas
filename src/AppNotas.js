import React, { useState, useEffect } from 'react';
import AgregarNota from './components/agregarNota/AgregarNota';
// import Buscador from './components/Buscador';
import LimpiarNotas from './components/LimpiarNotas';
import Lista from './components/Lista';
import './AppNotas.css';
// import Buscador from './components/Buscador';
// import LimpiarNotas from './components/LimpiarNotas';
// import fondo from './img/todo-list.jpg';

const AppNotas = () => {
    const [notasItems, setNotasItems] = useState([]);
    const [mostrarCompletado, setMostrarCompletado] = useState(false);

    // Crea una nota nueva
    const crearNota = (notaNueva) => {
        console.log(notaNueva);
        // Validamos si existe ya esa tarea para no repetirla
        if (!notasItems.find((nota) => nota.name === notaNueva)) {
            setNotasItems([
                ...notasItems,
                {
                    id: new Date().getTime().toString(),
                    name: notaNueva,
                    done: false,
                },
            ]);
        }
    };

    // Actualizar el status de la tarea
    const notaCompletada = (nota) => {
        setNotasItems(
            notasItems.map((n) =>
                n.name === nota.name ? { ...n, done: !n.done } : n
            )
        );
    };

    // Obtiene los datos de localStorage y los establece en el arreglo de notas
    useEffect(() => {
        let datos = localStorage.getItem('Notas');
        if (datos) {
            setNotasItems(JSON.parse(datos));
        }
    }, []);

    // Guarda los datos en LocalStorage
    useEffect(() => {
        localStorage.setItem('Notas', JSON.stringify(notasItems));
    }, [notasItems]);

    const limpiarNotasHechas = () => {
        setNotasItems(notasItems.filter((nota) => !nota.done));
        setMostrarCompletado(false);
    };

    const borrarNota = (nota) => {
        const borrada = notasItems.filter((n) => n.id !== nota.id);
        setNotasItems(borrada);
        localStorage.setItem('id', JSON.stringify(borrada));
    };

    // // Descargar App
    // const [isReadyForInstall, setIsReadyForInstall] = React.useState(false);

    // useEffect(() => {
    //     window.addEventListener('beforeinstallprompt', (event) => {
    //         // Prevent the mini-infobar from appearing on mobile.
    //         // event.preventDefault();
    //         console.log('👍', 'beforeinstallprompt', event);
    //         // Stash the event so it can be triggered later.
    //         window.deferredPrompt = event;
    //         // Remove the 'hidden' class from the install button container.
    //         setIsReadyForInstall(true);
    //     });
    // }, []);

    // // Botón instalación de PWA
    // async function downloadApp() {
    //     console.log('👍', 'butInstall-clicked');
    //     const promptEvent = window.deferredPrompt;
    //     if (!promptEvent) {
    //         // The deferred prompt isn't available.
    //         console.log('oops, no prompt event guardado en window');
    //         return;
    //     }
    //     // Show the install prompt.
    //     promptEvent.prompt();
    //     // Log the result
    //     const result = await promptEvent.userChoice;
    //     console.log('👍', 'userChoice', result);
    //     // Reset the deferred prompt variable, since
    //     // prompt() can only be called once.
    //     window.deferredPrompt = null;
    //     // Hide the install button.
    //     setIsReadyForInstall(false);
    // }

    return (
        <>
            {/* <LimpiarNotas />
            <Buscador /> */}
            {/* {isReadyForInstall && (
                <button onClick={downloadApp}>Descargar App</button>
            )} */}
            {/* <div>
                <img
                    className='img-fluid'
                    src={fondo}
                    alt='todo-list'
                    width={250}
                    lenght={250}
                />
            </div> */}
            {/* <Buscador /> */}
            <AgregarNota crearNota={crearNota} />

            <Lista
                notas={notasItems}
                notaCompletada={notaCompletada}
                borrarNota={borrarNota}
            />

            <LimpiarNotas
                isChecked={mostrarCompletado}
                setMostrarCompletado={(completado) =>
                    setMostrarCompletado(completado)
                }
                limpiarNotasHechas={limpiarNotasHechas}
            />

            {mostrarCompletado && (
                <Lista
                    notas={notasItems}
                    notaCompletada={notaCompletada}
                    mostrarCompletado={mostrarCompletado}
                    borrarNota={borrarNota}
                />
            )}
        </>
    );
};

export default AppNotas;
