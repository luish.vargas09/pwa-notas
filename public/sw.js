/* eslint-disable no-undef */
/* eslint-disable no-restricted-globals */
// importScripts('js/sw-utils');

const ESTATICO_CACHE = 'static-v1.0';
const INMUTABLE_CACHE = 'inmutable-v1.0';
const DINAMICO_CACHE = 'dynamic-v1.0';

const APP_SHELL = [
    '/',
    'favicon.ico',
    'index.html',
    'js/app.js',
    'manifest.json',
    'static/js/bundle.js',
    'img/fondo.jpg',
    'img/todo-list.jpg',
    'logo192.png',
    'logo512.png',
];

const APP_SHELL_INMUTABLE = [
    'https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css',
    'https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js',
];

self.addEventListener('install', (event) => {
    // Agrego el cache estático
    const cacheStatic = caches.open(ESTATICO_CACHE).then((cache) => {
        cache.addAll(APP_SHELL);
    });

    // Agrego el cache inmutable
    const cacheInmutable = caches
        .open(INMUTABLE_CACHE)
        .then((cache) => cache.addAll(APP_SHELL_INMUTABLE));

    // Esperamos hasta que lo anterior termine
    event.waitUntil(Promise.all[(cacheStatic, cacheInmutable)]);
});

// ACTIVACIÓN
self.addEventListener('activate', (event) => {
    // Eliminar cache del sw anterior
    const resp = caches.keys().then((keys) => {
        keys.forEach((key) => {
            if (key !== ESTATICO_CACHE && key.includes('static')) {
                return caches.delete(key);
            }
        });
    });

    event.waitUntil(resp);
});

self.addEventListener('fetch', (event) => {
    // Si encuentra algo en el cache lo regresa y guarda ese recurso
    const recurso = caches.match(event.request).then((resp) => {
        if (resp) return resp;

        // No existe el archivo... tengo que ir a la web >:(
        console.log(
            'No existe... tendré que ir a esta dirección: \n',
            event.request.url
        );

        // Hago un fecth a esa dirección para obtener el recurso
        return fetch(event.request.url).then((newResp) => {
            // El cache dinámico puede crecer demasiado
            caches.open(DINAMICO_CACHE).then((cache) => {
                cache.put(event.request.url, newResp);
            });
            return newResp.clone();
        });
    });
    event.respondWith(recurso);
});
